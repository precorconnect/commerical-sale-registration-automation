package tests

import modules.CommonModules;
import pages.CommercialRegPage
import pages.CommercialPricingPage
import pages.PrecorCustomerLoginPage
import pages.CommercialReviewPage
import pages.CommercialSubmitPage
import pages.HomePage
import pages.LoginPage
import pages.PrecorPremiumHomePage
import pages.PrecorUSAStagingLoginPage
import pages.CommercialDraftsPage
import geb.spock.GebReportingSpec
import geb.spock.GebSpec
import spock.lang.*

@Stepwise
class AddProdNotListedSpec extends GebReportingSpec {

	def config = new ConfigSlurper().parse(new File('src/test/groovy/Util/Config.groovy').toURL())
	
	def "Precor Login"(){
		
		given:
		to LoginPage
	
		when:
	//	report "Click Login"
		clickOnLogin()
		
		then:
		at PrecorCustomerLoginPage
		
		when:
		login_with(config.userName, config.password)
		//report "Provide valid credentials and Home page is displayed"

		then:
		at HomePage
	}
	
	def "PartnerTools Login"(){
		
		given:
		at HomePage

		when:
		selectPartnerTools()

		then:
		//report "Partner Tools Login screen is displayed"
		at PrecorUSAStagingLoginPage
	}
	
	def "PartnerTools"(){
		
		given:
		at PrecorUSAStagingLoginPage

		when:
		USAStagingSignIn(config.userName, config.password)

		then:
		//report "Partner Tools page is displayed"
		at PrecorPremiumHomePage
		
	}
	
	def "Commercial Reg Drafts"(){
		
		given:
		at PrecorPremiumHomePage

		when:
		ClickOnCommercialReg()

		then:
	//	report "Commercial Registration drafts page is displayed"
		at CommercialDraftsPage
	}
	
	def "Commercial Reg"(){
		
		given:
		at CommercialDraftsPage
		
		when:
		ClickOnNewRegistrationLink()
		
		then:
	//	report "Commercial Registration page is dispalyed"
		at CommercialRegPage
	
	}
	
	def "Add new prod not listed"(){
		
		given:
		at CommercialRegPage

		when:		
		fnVerifyAddProdNotListedPopup()
		report "Add product not listed popup is displayed"
		fnAddProdNotListed(config.addProdDet)

		then:	
		report "New Product created"
		
	}
	
}
