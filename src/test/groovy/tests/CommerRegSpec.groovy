package tests

import modules.CommonModules;
import pages.CommercialRegPage
import pages.CommercialPricingPage
import pages.PrecorCustomerLoginPage
import pages.CommercialReviewPage
import pages.CommercialSubmitPage
import pages.HomePage
import pages.LoginPage
import pages.PrecorPremiumHomePage
import pages.PrecorUSAStagingLoginPage
import pages.CommercialDraftsPage
import geb.spock.GebReportingSpec
import geb.spock.GebSpec
import spock.lang.*

@Stepwise
class CommerRegSpec extends GebReportingSpec {

	def config = new ConfigSlurper().parse(new File('src/test/groovy/Util/Config.groovy').toURL())
	
	def "Precor Login"(){
		
		given:
		to LoginPage
	
		when:	
		report "Click Login"
		clickOnLogin()
		
		then:		
		at PrecorCustomerLoginPage
		
		when:		
		login_with(config.userName, config.password)
		//report "Provide valid credentials and Home page is displayed"

		then:	
		at HomePage
	}
	
	def "PartnerTools Login"(){
		
		given:
		at HomePage

		when:
		selectPartnerTools()

		then:
		//report "Partner Tools Login screen is displayed"
		at PrecorUSAStagingLoginPage
	}
	
	def "PartnerTools page"(){
		
		given:
		at PrecorUSAStagingLoginPage

		when:
		USAStagingSignIn(config.userName, config.password)

		then:
		//report "Partner Tools page is displayed"
		at PrecorPremiumHomePage
		
	}
	
	def "Commercial Sale Registartion Drafts page"(){
		
		given:
		at PrecorPremiumHomePage

		when:
		ClickOnCommercialReg()

		then:
	//	report "Commercial Registration drafts page is displayed"
		at CommercialDraftsPage
	}
	
	def "Commercial Registartion page"(){
		
		given:
		at CommercialDraftsPage
		
		when:	
		ClickOnNewRegistrationLink()		
		
		then:
	//	report "Commercial Registration page is dispalyed"
		at CommercialRegPage
	
	}
	
	def "Save Facility Details"(){
		
		given:
		at CommercialRegPage
		
		when:
		fnEnterFacilityInfo(config.facilityName,config.contactEmaildet)		
		fnEnterSaleInfo(config.saleDet)
		fnAddProduct(config.assetsList)
	//	fnUploadInvoice()
		
		then:
	//	report "Pricing page is displayed"
		at CommercialPricingPage
		
	}
	
	def "Save Price details"(){
		
		given:
		at CommercialPricingPage
		
		when:
		fnEnterPriceforProducts(config.priceList)
		
		then:
	//	report "Review page is displayed"
		at CommercialReviewPage
				
	}
	
	def "Review screen"(){
		
		given:
		at CommercialReviewPage
					
		when:
		fnSubmitReview()
		
		then:
	//	report "Commercial Sale Reg is submitted"
		at CommercialReviewPage
		
	}
	
	def "Submitted screen"(){
		
		given:
		at CommercialReviewPage
		
		when:
		fnSubmitPageDisp()
		
		then: "Submit page is displayed"
		at CommercialSubmitPage
	}
	
	/*
	def cleanup() {
		if ($(id: "logout").isDisplayed()) {
			$(id: "logout").click()
			waitFor { $(id: "inputName").isDisplayed() }
		}
		driver.manage().deleteAllCookies()
	}*/


}