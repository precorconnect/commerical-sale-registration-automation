package Util

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory;;

class ExcelReader {

	def readData(String sheetName, String TCName) {
		def path = "E:\\Automation-WorkArea\\APITest\\Book1.xlsx";
		InputStream inputStream = new FileInputStream(path);
		Workbook workbook = WorkbookFactory.create(inputStream);
		Sheet sheet = workbook.getSheetAt(sheetName);

		Iterator rowIterator = sheet.rowIterator();
		rowIterator.next()
		Row row;
		def rowsData = []
		while(rowIterator.hasNext()) {
			row = rowIterator.next()
			def rowIndex = row.getRowNum()
			def colIndex;
			def rowData = []
			for (Cell cell : row) {
				colIndex = cell.getColumnIndex()
				rowData[colIndex] = cell.getRichStringCellValue().getString();
			}
			rowsData << rowData
		}
		rowsData
	}
}

def myTestCase = context.testCase

ReadExcel excelReader = new ReadExcel();
List rows = excelReader.readData();
def d = []
Iterator i = rows.iterator();
while( i.hasNext()){
	d = i.next();
	myTestCase.setPropertyValue("From", d[0])
	myTestCase.setPropertyValue("To", d[1])
	testRunner.runTestStepByName( "ConversionRate")
}
