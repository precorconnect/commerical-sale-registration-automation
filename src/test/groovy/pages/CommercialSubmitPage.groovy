package pages

import geb.Page

class CommercialSubmitPage extends Page {

		static at = {waitFor{title == "Precor - Premium Home and Commercial Fitness Equipment"}}	
		static content = {
			
			btnStartNewReg {$(".form-group").find(".btn.btn-sm").find("button",type:"reset")}		
			
		}
		
		Void fnSubmitPageDisp()
		{
			//Wait for Start new registration button		
			
			waitFor{btnStartNewReg.displayed}
			Thread.sleep(500)
		}
		
}
