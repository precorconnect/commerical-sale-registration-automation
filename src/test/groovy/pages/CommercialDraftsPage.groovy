package pages

import geb.Page

class CommercialDraftsPage extends Page {
	
	static at={waitFor{$("#draft_table").find("tr",1).displayed}}
		//title=="Precor - Premium Home and Commercial Fitness Equipment"}}
		
	static content= {
		
		newRegistrationBtn {$(".row.col-sm-6").find(".btn.btn-primary")}
		/*backToPartnerTools = {$("a").find(".btn btn-sm btn-primary")};
		
		deleteYes = {$("div.modal-footer ng-scope","button.btn btn-primary")}
		deleteNo = {$("div.modal-footer ng-scope","button.btn btn-warning")}*/
		draftTbl {$("#draft_table")}			//.find("a",0)} 
	}
		
	 void ClickOnNewRegistrationLink() {
		waitFor{draftTbl.displayed}
		Thread.sleep(3000)
		
		waitFor{newRegistrationBtn.displayed}
		newRegistrationBtn.click()
		println "New Registration Clicked"
	}
	
	/*void ClickOnPartnerToolsLink() {
		backToPartnerTools.click()
		
	}
	
	Void DeleteDraftYes(int RegId)
	{
		system.out.println(Regid)
		//deleteYes.click()
	}
	
	Void verifyDraftDeleted(int RegId)
	{
		//
	}
	
	Void DeleteDraftNo()
	{
		deleteNo.click()
	}*/
}


