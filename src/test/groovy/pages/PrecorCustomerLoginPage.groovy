package pages

import geb.Page

class PrecorCustomerLoginPage extends Page {
	static at = {waitFor{title == "Customer Login"}}

	static content = {
		username {$("#singlepoint-username")}
		passwd {$("#singlepoint-password")}
		login(to:HomePage) {$("#send2")}
	}

	void login_with(String userName, String password) {
		username.value(userName)
		passwd.value(password)
		login.click()
	}
}
