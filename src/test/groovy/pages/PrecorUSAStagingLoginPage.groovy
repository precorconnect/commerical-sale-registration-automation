package pages

import geb.Page

class PrecorUSAStagingLoginPage extends Page {
	static at = {waitFor{title == "Precor USA Staging - Sign In"}}

	static content = {
		username {$("#user-signin")}
		passwd {$("#pass-signin")}
		signin {$("#signin-button")}
	}

	void USAStagingSignIn(String userName, String password) {
		username.value(userName)
		passwd.value(password)
		signin.click()
		
			}
}
