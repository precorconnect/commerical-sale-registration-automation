package pages

import geb.Page

class CommercialPricingPage extends Page {
	
	static at={ waitFor{ title=="Precor - Premium Home and Commercial Fitness Equipment"}}
	
	static content= {
	
		txtSinglePrice {$("form",name:"productPricingForm").find("input",name:"simplePrice")}
		txtComponentPrice {$("form",name:"productPricingForm").find("input",name:"componentPrice")}
		txtCompositePrice {$("form",name:"productPricingForm").find("input",name:"compositePrice")}
		nextBtn {$("form",name:"productPricingForm").find(".form-group").find(".btn.btn-sm.btn-primary")}
		unitPriceChk {$("form",name:"productPricingForm").find("input",type:"checkbox")}
	
		okBtn {$(".modal.fade.ng-isolate-scope").find(".modal-footer.ng-scope").find(".btn.btn-primary")}
		
		}
	
	Void fnEnterPriceforProducts(String priceList){
		//waitFor{txtSinglePrice.displayed}
		String[] priceLst = priceList.split("&")
		
		def var,i=0,j=0,k=0,cval1,cval2,sumVal
		def priceLength = priceLst.size()
		println "Price list size: " + priceLength
			
		for(var=0;var<priceLength;var++)		
		{
						
			if(priceLst[var].contains(";"))
			{
				String[] priceval=priceLst[var].split(",")
				def unitPriceVal=priceval[1]
				String[] compositePriceVal=priceval[0].split(";")
				
				cval1=compositePriceVal[0]
				cval2=compositePriceVal[1]
				sumVal=cval1.toInteger() + cval2.toInteger()
				if(unitPriceVal=="1") 
				{
					unitPriceChk.find(k++).click()
					Thread.sleep(500)
					txtCompositePrice.find(j++).value(sumVal)
				
				}
				else
				{				
					txtComponentPrice.find(j++).value(cval1)
					Thread.sleep(500)
					txtComponentPrice.find(j++).value(cval2)
				}
			}
			else
			{
				waitFor{txtSinglePrice.displayed}
				def singlePriceVal=priceLst[var]
				txtSinglePrice.find(i++).value(singlePriceVal)
				Thread.sleep(1000)
			}
		}
		
		//click Next button
		nextBtn.click()
		//Thread.sleep(3000)
		waitFor{okBtn.displayed}
		okBtn.click()
		Thread.sleep(3000)
	}

}
