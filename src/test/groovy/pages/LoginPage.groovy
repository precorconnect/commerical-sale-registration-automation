package pages

import geb.Page

class LoginPage extends Page {
	static url = "http://dev.precorconnect.com/"
	static at = {title == "Precor"}

	static content = {
		loginLink {$("#stand_alone_block",0).find("a",0)}
	}

	void clickOnLogin() {
		loginLink.click()
	}
	
}
