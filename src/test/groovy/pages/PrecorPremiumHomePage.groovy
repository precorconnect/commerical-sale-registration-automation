package pages

import geb.Page

class PrecorPremiumHomePage extends Page {
	static at = {waitFor{$(".block",0).find("h2").displayed}}
//	title == "Precor - Premium Home and Commercial Fitness Equipment"
	static content = {
	//	heading {$(".block-wrapper.ng-scope").find("h2",0)}
		CommercialRegLink {$(".block",0).find("a",0)}
	}
	
	
	void ClickOnCommercialReg() {
		waitFor{CommercialRegLink.displayed}
		println driver.getTitle();
		CommercialRegLink.click()
		
	}
}
