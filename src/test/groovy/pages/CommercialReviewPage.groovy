package pages

import geb.Page

class CommercialReviewPage extends Page {
	static at = {waitFor{$(".table-responsive").displayed}}
		//title == "Precor - Premium Home and Commercial Fitness Equipment"}}

	static content = {
		
		btnPartnerTools {$(".pull-right").find("button",type:"submit")}
		btnSubmit {$(".form-group").find("button",type:"submit")}
		btnPrev {$(".form-group").find("button",type:"reset")}
		btnOk {$(".modal-dialog.modal-sm").find(".modal-footer.ng-scope").find(".btn.btn-primary")}
	}

	Void fnNavigateToPartnerTools()
	{
		waitFor{btnPartnerTools.displayed}
		btnPartnerTools.click()
		//Thread.sleep(500)
	}
	
	Void fnSubmitReview()
	{
	//	waitFor
		waitFor{btnSubmit.displayed}
		btnSubmit.click()	
		Thread.sleep(1000)	
		
		//Click on Ok button
		waitFor{btnOk.displayed}
		btnOk.click()
		Thread.sleep(500)
	}
	
	Void fnReviewPrev()
	{
		waitFor{btnPrev.displayed}
		btnPrev.click()
	//	Thread.sleep(500)
	}

}
