/*
 This is the Geb configuration file.
 See: http://www.gebish.org/manual/current/configuration.html
 */

import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.chrome.ChromeDriver


// See: http://code.google.com/p/selenium/wiki/HtmlUnitDriver

driver = { def driverInstance = new FirefoxDriver()
    driverInstance.manage().window().maximize()
    driverInstance
 }

waiting {
    timeout = 30
  
   }

//reportsDir = new File("target/test-reports")
String frmt = new Date().format("ddMMyyy") + "_" + new Date().getTime()
reportsDir = new File("D:/Services/Automation/Results/"+ frmt)

environments {

	// run as “mvn -Dgeb.env=chrome test”
	// See: http://code.google.com/p/selenium/wiki/ChromeDriver
/*	chrome {
		driver = { new ChromeDriver() }
	}*/

}